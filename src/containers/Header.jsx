import React from 'react';
import {NavLink} from 'react-router-dom';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { useAuth } from '../context/AuthContext';
import { useEffect } from 'react';

function Header() {
    const { isUserLoggedIn, logout, isUserAdmin } = useAuth();

    useEffect(() => {
        console.log(isUserLoggedIn);
    }, [isUserLoggedIn]);

    return (
        <nav className="navbar">
            <NavLink className="navbar-brand nl" to="/">
                <img className="brand-img" width="60px" height="70px" src="https://cdn-icons-png.flaticon.com/512/1698/1698293.png" alt="brand"/>
                <h1>Energy Management System</h1>
            </NavLink>
            <ul className="navbar-nav ml-auto">
                {
                    isUserLoggedIn ? 
                        isUserAdmin ? 
                            <span className="navbar-admin-links">
                                <li className="nav-item active">
                                    <NavLink to='/users' className="nav-link">
                                        Users
                                    </NavLink>
                                </li>
                                <li className="nav-item active">
                                    <NavLink to='/devices' className="nav-link">
                                       Devices
                                    </NavLink>
                                </li>
                            </span>
                            : <li className="nav-item active">
                                <NavLink to='/profile' className="nav-link">
                                    Profile
                                </NavLink> </li>
                        : <li className='nav-item active'>
                            <NavLink to='/login' className="nav-link">
                                    Login
                            </NavLink>
                        </li>
                }
                {
                    isUserLoggedIn ? <li className="nav-item active">
                        <NavLink to={isUserLoggedIn ? '/' : '/'} className="nav-link" onClick={logout}>
                            <ExitToAppIcon/>
                        </NavLink>
                    </li> : null
                }
            </ul>
        </nav>
    );
}

export default Header;