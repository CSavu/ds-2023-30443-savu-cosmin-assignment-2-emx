import React from 'react';

function Footer() {
    return (
        <section id="Footer">
            <p><a target="_blank" rel="noopener noreferrer" href="https://github.com/CSavu/">Cosmin Savu</a> 2023</p>
        </section>
    );
}

export default Footer;