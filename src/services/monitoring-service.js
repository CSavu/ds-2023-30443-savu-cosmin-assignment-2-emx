import { getCookie } from '../Utils';

export const getAllReadingsForDeviceOnDate = async (deviceId, date) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8445/api/readings/getAllForDeviceOnDate?deviceId=${deviceId}&date=${date}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token'),
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};