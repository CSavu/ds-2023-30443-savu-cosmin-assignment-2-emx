import { getCookie } from '../Utils';

export const getAllDevicesForUser = async (userId) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8444/api/device/getAllForUser?userId=${userId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token'),
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const getAllDevices = async () => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8444/api/device/getAll`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const getById = async (deviceId) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8444/api/device/getById?deviceId=${deviceId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
        throw error;
    }
};

export const createDevice = async (name, address, maximumHourlyEnergyConsumption) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8444/api/device/create`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            },
            body: JSON.stringify({ name, address, maximumHourlyEnergyConsumption })
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const updateDevice = async (id, name, address) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8444/api/device/update`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            },
            body: JSON.stringify({ id, name, address })
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const deleteDevice = async (deviceId) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8444/api/device/delete?deviceId=${deviceId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const addDeviceToUser = async (deviceId, userId) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8444/api/device/addDeviceToUser?deviceId=${deviceId}&userId=${userId}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};