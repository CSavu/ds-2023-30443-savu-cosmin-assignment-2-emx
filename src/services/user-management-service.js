import { getCookie } from '../Utils';

export const getAllUsers = async () => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8443/api/user/getAll`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const createUser = async (username, role, password) => {
    try {
        const requestBody = JSON.stringify({ username, password, role });

        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8443/api/user/create`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            },
            body: requestBody
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const deleteUser = async (userId) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8443/api/user/delete?userId=${userId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const updateUser = async (username, role) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8443/api/user/update`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            },
            body: JSON.stringify({ username, role })
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};

export const getUserById = async (userId) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8443/api/user/getById?userId=${userId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token')
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};