import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { useAuth } from '../context/AuthContext';
import { getAllUsers, deleteUser, createUser, updateUser } from '../services/user-management-service';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '@material-ui/core/TextField';
import AdminChatPopup from '../components/AdminChatPopup';

function ListItemLink(props) {
    return <ListItem button component={Link} {...props} />;
}

function UserDashboard() {
    const { isUserLoggedIn, refreshAuthorization, isUserAdmin, authorizedUserId, isAuthInProgress } = useAuth();
    //const { user } = {};
    const [users, setUsers] = useState([]);
    const [username, setUsername] = useState('');
    const [role, setRole] = useState('');
    const [password, setPassword] = useState('');
    const [socket, setSocket] = useState(null);

    async function getUsers() {
        const usersResponse = await getAllUsers();
        setUsers(usersResponse);
        console.log(usersResponse);
    }

    function handleUsernameChange(event) {
        const newVal = event.target.value;
    
        setUsername(newVal);
    }
    
    function handleRoleChange(event) {
        const newVal = event.target.value;
    
        setRole(newVal);
    }

    function handlePasswordChange(event) {
        const newVal = event.target.value;
    
        setPassword(newVal);
    }

    const [editedRoles, setEditedRoles] = useState({});

    const handleEditClick = (userId, role) => {
        setEditedRoles({
            ...editedRoles,
            [userId]: role,
        });
    };

    const handleSaveClick = async (userId) => {
        const editedRole = editedRoles[userId];
        
        console.log(`Save user ${userId} with role: ${editedRole}`);

        const updateResponse = await updateUser(users.filter(u => u.id === userId)[0].username, editedRole);

        console.log(updateResponse);

        if (updateResponse?.id) {
            setEditedRoles((prevEditedRoles) => {
                const { [userId]: removedRole, ...rest } = prevEditedRoles;
                console.log(removedRole);
                return rest;
            });
    
            setUsers((prevUsers) => {
                return [
                    ...prevUsers.filter(u => u.id !== userId),
                    {
                        ...prevUsers.filter(u => u.id === userId)[0],
                        role: editedRole
                    }
                ];
            });
        } else {
            alert('Invalid input!');
        }
    };

    const handleCancelClick = (userId) => {
        setEditedRoles((prevEditedRoles) => {
            const { [userId]: removedRole, ...rest } = prevEditedRoles;
            console.log(removedRole);
            return rest;
        });
    };

    async function handleUserAdd(event) {
        event.preventDefault();
        const creationResponse = await createUser(username, role, password);

        console.log(creationResponse);

        if (creationResponse?.id) {
            setUsers([...users, {
                id: creationResponse.id,
                username: creationResponse.username,
                role: creationResponse.role
            }]);
        } else {
            alert('Invalid input!');
        }

        setUsername('');
        setRole('');
        setPassword('');
    }

    useEffect(() => {
        refreshAuthorization();
    }, []);

    useEffect(() => {
        if (isUserLoggedIn && isUserAdmin) {
            getUsers();
        }
    }, [isUserLoggedIn, isUserAdmin]);

    useEffect(() => {
        if (authorizedUserId) {
            setSocket(new WebSocket(`wss://${process.env.REACT_APP_API_HOST}:8446/ws/admin-chat?userId=${authorizedUserId}`));
        }
    }, [authorizedUserId]);

    if (!isAuthInProgress && isUserLoggedIn && isUserAdmin) {
        return (
            <section id="users">
                <div className="container">
                    <h2>Existing users</h2>
                    <List>
                        {users?.map((user, index) => (
                            user.username && (
                                <div key={index}>
                                    <ListItem>
                                        <ListItemLink>
                                            <ListItemText className="list-item-text"><strong>Id:</strong> {user.id}</ListItemText>
                                            <ListItemText className="list-item-text"><strong>Username:</strong> {user.username}</ListItemText>
                                            {editedRoles[user.id] !== undefined ? (
                                                <TextField
                                                    value={editedRoles[user.id]}
                                                    onChange={(e) => setEditedRoles({ ...editedRoles, [user.id]: e.target.value })}
                                                    label="Edit Role"
                                                />
                                            ) : (
                                                <ListItemText className="list-item-text"><strong>Role:</strong> {user.role}</ListItemText>
                                            )}
                                        </ListItemLink>
                                        {editedRoles[user.id] !== undefined ? (
                                            <>
                                                <IconButton
                                                    onClick={() => handleSaveClick(user.id)}
                                                    type="button"
                                                    aria-label="save"
                                                    color="primary"
                                                >
                                                    <SaveIcon fontSize="small" />
                                                </IconButton>
                                                <IconButton
                                                    onClick={() => handleCancelClick(user.id)}
                                                    type="button"
                                                    aria-label="cancel"
                                                    color="secondary"
                                                >
                                                    <CloseIcon fontSize="small" />
                                                </IconButton>
                                            </>
                                        ) : (
                                            <>
                                                <IconButton
                                                    onClick={() => handleEditClick(user.id, user.role)}
                                                    type="button"
                                                    aria-label="edit"
                                                    color="primary"
                                                    style={{ float: 'right' }}
                                                >
                                                    <EditIcon fontSize="small" />
                                                </IconButton>
                                                <IconButton
                                                    onClick={() => {
                                                        console.log(user);

                                                        async function handleScrap() {
                                                            const response = await deleteUser(user.id);
                                                            const data = response;

                                                            if (data === true) setUsers(users.filter(u => u.id !== user.id));
                                                            else alert('You do not have the necessary permissions for this action!');
                                                        }
                                                        // eslint-disable-next-line no-restricted-globals
                                                        let result = confirm('Are you sure you want to delete this user?');
                                                        if (result === true) {
                                                            handleScrap();
                                                        }
                                                    }}
                                                    type="button"
                                                    aria-label="delete"
                                                    color="primary"
                                                    style={{ float: 'right' }}
                                                >
                                                    <DeleteIcon fontSize="small" />
                                                </IconButton>
                                            </>
                                        )}
                                    </ListItem>
                                    <Divider />
                                </div>
                            )
                        ))}
                    </List>
                </div>
                <br/><br/>
                <section id="add-user">
                    <div className="container">
                        <form type="submit" className="work-form" onSubmit={handleUserAdd}>
                            <h2>Create new user</h2>
                            <TextField label="Username" className="work-form" onChange={handleUsernameChange} type="text" autoComplete="off"
                                name="newUsername" value={username}/>
                            <br/><br/>
                            <TextField label="Role" className="work-form" onChange={handleRoleChange} type="text" autoComplete="off"
                                name="newRole" value={role}/>
                            <br/><br/>
                            <TextField label="Password" className="work-form" onChange={handlePasswordChange} type="password" autoComplete="off"
                                name="newPassword" value={password}/>
                            <Fab
                                style={{ marginTop: '8px'}}
                                id="work-fab"
                                type="button"
                                aria-label="add"
                                color="primary"
                                size="small"
                                onClick={handleUserAdd}
                            >
                                <AddIcon/>
                            </Fab>
                            <br/><br/>
                        </form>
                    </div>
                </section>
                <AdminChatPopup socket={socket} userId={authorizedUserId} />
            </section>
        );
    } else if (!isAuthInProgress) {
        window.location.href = '/login';
        return null;
    }
}

export default UserDashboard;
