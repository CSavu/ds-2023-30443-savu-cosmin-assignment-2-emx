import { React, useState, useEffect } from 'react';
import './UserChatPopup.css';
import SendIcon from '@mui/icons-material/Send';
import { generateUid } from '../Utils';

function UserChatPopup({ socket, userId }) {
    const [isChatOpen, setIsChatOpen] = useState(false);
    const [messages, setMessages] = useState([]);
    const [newMessage, setNewMessage] = useState('');
    const [isTyping, setIsTyping] = useState(false);
    const [filteredMessages, setFilteredMessages] = useState([]);

    const handleSendMessage = (message) => {
        if (message) {
            const uid = generateUid();
            const timestamp = new Date().getTime() / 1000;
            setMessages(prevMessagee => [...prevMessagee, { timestamp, uid, message, sender: 'user' }]);
            socket.send(JSON.stringify({ timestamp, uid, message, isTyping: false, senderUserId: userId  }));
            setNewMessage('');
        } 
    };

    const handleOpenChat = () => {
        if (!isChatOpen) {
            messages.forEach(msg => {
                if (msg.sender === 'admin' && !msg.read && msg.message) {
                    socket.send(JSON.stringify({ uid: msg.uid, isTyping: false, senderUserId: userId, read: true, timestamp: msg.timestamp }));
                }
            });
        }
        setIsChatOpen(!isChatOpen);
    };

    const handleChange = (event) => {
        setNewMessage(event.target.value);
        socket.send(JSON.stringify({ isTyping: true, senderUserId: userId }));
    };

    const handleTypingStart = () => {
        socket.send(JSON.stringify({ isTyping: true, senderUserId: userId }));
    };

    const handleOnBlur = () => {
        socket.send(JSON.stringify({ isTyping: false, senderUserId: userId }));
    };

    useEffect(() => {
        if (socket != null) {    
            socket.onmessage = (event) => {
                const receivedMessage = JSON.parse(event.data);
                console.log('Received message on admin chat:', receivedMessage);
                console.log(messages);

                if (receivedMessage.read && receivedMessage.receiverUserId === userId && messages.map(msg => msg.uid).includes(receivedMessage.uid)) {
                    setMessages(prevMessages => {
                        return [
                            ...prevMessages.filter(msg => msg.uid !== receivedMessage.uid),
                            ...prevMessages
                                .filter(msg => msg.uid === receivedMessage.uid)
                                .map(msg => {
                                    console.log(msg);
                                    return { ...msg, read: true };
                                })
                        ];
                    });
                } else if (receivedMessage.message) {
                    setMessages(prevMessages => [
                        ...prevMessages,
                        { timestamp: receivedMessage.timestamp, uid: receivedMessage.uid, message: receivedMessage.message, sender: 'admin', senderUserId: receivedMessage.senderUserId, receiverUserId: receivedMessage.receiverUserId },
                    ]);

                    if (isChatOpen) {
                        socket.send(JSON.stringify({ uid: receivedMessage.uid, isTyping: false, senderUserId: userId, read: true }));
                    }
                }
                
                console.log(messages);

                if (receivedMessage.isTyping) {
                    setIsTyping(true);
                } else if (receivedMessage.isTyping === false) {
                    setIsTyping(false);
                }
            };
        }
    }, [socket, messages, isChatOpen]);

    useEffect(() => {
        if (socket !== null) {
            socket.onopen = () => {
                console.log('Admin socket connection opened');
            };
    
            socket.onclose = () => {
                console.log('Admin socket connection closed');
            };
    
            // Clean up the WebSocket connection on component unmount
            return () => {
                socket.close();
            };
        }
    }, [socket]);

    useEffect(() => {
        setFilteredMessages(messages.sort((a, b) => a.timestamp - b.timestamp));
    }, [messages]);
  
    return (
        <div>
            <button
                onClick={handleOpenChat}
                className="chat-button"
            >
          Chat with Admin
            </button>
            {isChatOpen && (
                <div className='chat-box'>
                    <div className='chat-title'>
                        Chat with Admin
                    </div>
                    <div className='chat-messages'>
                        {filteredMessages.map((msg, index) => (
                            <div key={index} style={{ marginBottom: '10px', textAlign: msg.sender === 'admin' ? 'left' : 'right' }}>
                                <span className='chat-message'
                                    style={{
                                        backgroundColor: msg.sender === 'admin' ? '#f0f0f0' : '#5e00da',
                                        color: msg.sender === 'admin' ? '#000' : '#fff',
                                        padding: '8px',
                                        borderRadius: '8px',
                                        display: 'inline-block',
                                        maxWidth: '80%',
                                    }}
                                >
                                    {msg.message}
                                    { msg.sender === 'user' && (msg.read ? <div>Read</div> : <div>Sent</div>) }
                                </span>
                            </div>
                        ))}
                        { isTyping && <div style={{ marginBottom: '10px', textAlign: 'left' }}> Admin is typing... </div> }
                    </div>
                    <div style={{ padding: '10px' }}>
                        <input
                            type="text"
                            placeholder="Type your message..."
                            className='chat-input'
                            onChange={handleChange}
                            onInput={handleTypingStart}
                            value={newMessage}
                            onKeyDown={(e) => {
                                if (e.key === 'Enter') {
                                    handleSendMessage(e.target.value);
                                    e.target.value = '';
                                }
                            }}
                            onBlur={handleOnBlur}
                        />
                        <SendIcon style={{ marginLeft: '8px', cursor: 'pointer', color: '#5e00da' }} onClick={() => {
                            const input = document.querySelector('.chat-input');
                            handleSendMessage(input.value);
                            input.value = '';
                        }}
                        />
                    </div>
                </div>
            )}
        </div>
    );
}

export default UserChatPopup;