import { React, useState, useEffect } from 'react';
import './AdminChatPopup.css';
import SendIcon from '@mui/icons-material/Send';
import { generateUid } from '../Utils';

function AdminChatPopup({ socket, userId }) {
    const [isChatOpen, setIsChatOpen] = useState(false);
    const [messages, setMessages] = useState([]);
    const [newMessage, setNewMessage] = useState('');
    const [isTyping, setIsTyping] = useState(false);
    const [users, setUsers] = useState([]);
    const [selectedUser, setSelectedUser] = useState(null);
    const [filteredMessages, setFilteredMessages] = useState([]);

    const handleSendMessage = (message) => {
        if (message) {
            const uid = generateUid();
            const timestamp = new Date().getTime() / 1000;
            setMessages(prevMessages => [...prevMessages, { timestamp, uid, message, sender: 'admin', senderUserId: userId, receiverUserId: selectedUser }]);
            socket.send(JSON.stringify({ timestamp, uid, message, isTyping: false, senderUserId: userId, receiverUserId: selectedUser, senderRole: 'ADMIN'  }));
            setNewMessage('');
        }
    };

    const handleChange = (event) => {
        setNewMessage(event.target.value);
        socket.send(JSON.stringify({ isTyping: true, senderUserId: userId, receiverUserId: selectedUser }));
    };

    const handleTypingStart = () => {
        socket.send(JSON.stringify({ isTyping: true, senderUserId: userId, receiverUserId: selectedUser }));
    };

    const handleUserClick = (user) => {
        console.log(user);
        if (user !== null) {
            setSelectedUser(user.userId);
            messages.forEach(msg => {
                if (msg.sender === 'user' && !msg.read) {
                    socket.send(JSON.stringify({ timestamp: msg.timestamp, uid: msg.uid, isTyping: false, senderUserId: userId, receiverUserId: user.userId, read: true }));
                }
            });
        }
    };

    const handleChatChange = () => {
        setIsChatOpen(!isChatOpen);
        setSelectedUser(null);
    };

    const handleOnBlur = () => {
        socket.send(JSON.stringify({ isTyping: false, senderUserId: userId, receiverUserId: selectedUser }));
    };

    useEffect(() => {
        if (selectedUser !== null) {
            setFilteredMessages(messages.filter((msg) => msg.senderUserId === selectedUser || msg.receiverUserId === selectedUser).sort((a, b) => a.timestamp - b.timestamp));
        } else {
            setFilteredMessages([]);
        }
    }, [selectedUser, messages]);

    useEffect(() => {
        if (socket !== null) {
            socket.onopen = () => {
                console.log('Admin socket connection opened');
            };

            socket.onclose = () => {
                console.log('Admin socket connection closed');
            };
    
            // Clean up the WebSocket connection on component unmount
            return () => {
                socket.close();
            };
        }
    }, [socket]);

    useEffect(() => {
        if (socket != null) {
            socket.onmessage = (event) => {
                const receivedMessage = JSON.parse(event.data);
                const receivedMessageUserId = receivedMessage.senderUserId;
                
                setUsers((prevUsers) => {
                    if (userId !== receivedMessageUserId && !prevUsers.map(user => user.userId).includes(receivedMessageUserId)) {
                        return [...prevUsers, { userId: receivedMessageUserId, username: receivedMessage.senderUsername, role: receivedMessage.senderRole }];
                    }
                    return prevUsers;
                });

                console.log(receivedMessage.read);
                console.log(messages);
                if (receivedMessage.read && messages.map(msg => msg.uid).includes(receivedMessage.uid)) {
                    console.log('here');
                    setMessages(prevMessages => {
                        return [
                            ...prevMessages.filter(msg => msg.uid !== receivedMessage.uid),
                            ...prevMessages
                                .filter(msg => msg.uid === receivedMessage.uid)
                                .map(msg => {
                                    return { ...msg, read: true };
                                })
                        ];
                    });
                } else if (receivedMessage.message) {
                    setMessages(prevMessages => [
                        ...prevMessages,
                        { timestamp: receivedMessage.timestamp, uid: receivedMessage.uid, message: receivedMessage.message, sender: receivedMessage.senderRole === 'REGULAR_USER' ? 'user' : 'admin', senderUserId: receivedMessageUserId, receiverUserId: receivedMessage.receiverUserId },
                    ]);
                    socket.send(JSON.stringify({ timestamp: receivedMessage.timestamp, uid: receivedMessage.uid, isTyping: false, senderUserId: userId, receiverUserId: selectedUser, read: true }));
                }
                
                if (receivedMessage.isTyping && receivedMessage.senderUserId === selectedUser) {
                    setIsTyping(true);
                } else if (receivedMessage.isTyping === false && receivedMessage.senderUserId === selectedUser) {
                    setIsTyping(false);
                }
            };
        }
    }, [socket, selectedUser, filteredMessages, messages, userId]);
  
    return (
        <div>
            <button
                onClick={handleChatChange}
                className="admin-chat-button"
            >
                Chat with Users   
            </button>
            {isChatOpen && (
                <div className='admin-chat-box'>
                    <div className='admin-chat-title'>
                        Chat with Users
                    </div>
                    <div className='admin-chat-box-content'>
                        <div className='admin-chat-box-left-column'>
                            <div className="user-list">
                                <h4>User List</h4>
                                <ul>
                                    {users.map((user, index) => (
                                        <li key={index} onClick={() => handleUserClick(user)}>
                                            {`${user.username}`}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>

                        <div className='admin-chat-box-right-column'>
                            <div className='admin-chat-messages'>
                                {filteredMessages.map((msg, index) => (
                                    <div key={index} style={{ marginBottom: '10px', textAlign: msg.sender === 'user' ? 'left' : 'right' }}>
                                        <span className='admin-chat-message'
                                            style={{
                                                backgroundColor: msg.sender === 'user' ? '#f0f0f0' : '#5e00da',
                                                color: msg.sender === 'user' ? '#000' : '#fff',
                                                padding: '8px',
                                                borderRadius: '8px',
                                                display: 'inline-block',
                                                maxWidth: '80%',
                                            }}
                                        >
                                            { msg.message }
                                            { msg.sender === 'admin' && (msg.read ? <div>Read</div> : <div>Sent</div>) }
                                        </span>
                                    </div>
                                ))}
                                { isTyping === true && <div style={{ marginBottom: '10px', textAlign: 'left' }}> User is typing... </div> }
                            </div>
                            <div style={{ padding: '10px' }} className='admin-chat-input-container'>
                                <input
                                    type="text"
                                    placeholder="Type your message..."
                                    className='admin-chat-input'
                                    onChange={handleChange}
                                    onInput={handleTypingStart}
                                    value={newMessage}
                                    onKeyDown={(e) => {
                                        if (e.key === 'Enter') {
                                            handleSendMessage(e.target.value);
                                            e.target.value = '';
                                        }
                                    }}
                                    onBlur={handleOnBlur}
                                />
                                <SendIcon style={{ marginLeft: '8px', cursor: 'pointer', color: '#5e00da' }} onClick={() => {
                                    const input = document.querySelector('.admin-chat-input');
                                    handleSendMessage(input.value);
                                    input.value = '';
                                }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}

export default AdminChatPopup;