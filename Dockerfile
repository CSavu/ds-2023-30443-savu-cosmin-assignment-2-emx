FROM node:14.15.1 as builder
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build:docker

FROM nginx:1.17-alpine
RUN apk --no-cache add curl
RUN curl -L https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin

COPY --from=builder /app/nginx.conf /etc/nginx/nginx.template

# Set up SSL/TLS
COPY ./certs/nginx.crt /etc/nginx/nginx.crt
COPY ./certs/nginx.key /etc/nginx/nginx.key

RUN chmod 644 /etc/nginx/nginx.crt
RUN chmod 644 /etc/nginx/nginx.key

CMD ["/bin/sh", "-c", "envsubst < /etc/nginx/nginx.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]
COPY --from=builder /app/build/ /usr/share/nginx/html